# -*- coding: UTF-8 -*-
import tornado.web
import os
import config


class IndexHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):  # 处理get请求
        self.write("Hello world!")  # 给浏览器响应信息


class HomeHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        # self.write("Home")
        self.render("index.html")


# 文件上传
class UpFileHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.render("upfile.html")  # 页面加载

    def post(self, *args, **kwargs):
        # 文件字典对象
        files_dic = self.request.files
        # 将字典对象保存到本地
        for filename in files_dic:
            file_arr = files_dic[filename]
            for file_obj in file_arr:
                # 存储路径
                file_path = os.path.join(config.BASE_DIRS, 'upfiles/' + file_obj.filename)
                # 数据写入
                with open(file_path, "wb") as f:
                    f.write(file_obj.body)
        self.write("上传成功")

