# -*- coding: UTF-8 -*-
import tornado.ioloop
import tornado.httpserver
import config
from application import Application


def main():
    # 实例化对象
    app = Application()
    httpServer = tornado.httpserver.HTTPServer(app)
    # httpServer.bind(config.options.port) 报错，可使用下面两种方法
    # httpServer.bind(config.port())
    httpServer.bind(config.options['port'])
    httpServer.start(1)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()