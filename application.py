# -*- coding: UTF-8 -*-
import tornado.web
import config
from views import index


class Application(tornado.web.Application):
    def __init__(self):  # 继承tornado.web调用tornado.web.Application的__init__方法
        handlers = [
            (r'/', index.IndexHandler),
            (r'/home', index.HomeHandler),

            # 文件上传
            (r'/upfile', index.UpFileHandler),
        ]
        super(Application, self).__init__(handlers, **config.settings)  # 调用父类init方法
