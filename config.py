# -*- coding:UTF-8 -*-
import os

BASE_DIRS = os.path.dirname(__file__)  # 得到当前文件的路径

# 参数

options = {
    "port": 9000
}


def port():  # option部分报错,也可调用函数返回端口值
    return 9000


# 配置

settings = {
    "static_path": os.path.join(BASE_DIRS, "static"),
    "template_path": os.path.join(BASE_DIRS, "templates"),
    "debug": True,  # 设置tornado是否工作在调试状态下，默认False
    # True 的特性：
    # 1、自动重启(也可使用autoreload=True 单独设置)、
    # 2、取消缓存编译模板(也可使用compiled_template_cache=False 单独设置)、
    # 3、取消缓存静态文件的hash值(也可使用static_hash_cache=False 单独设置)、
    # 4、提供追踪信息(也可使用serve_traceback=False 单独设置)
}
