# tornado 创建的基础项目模板
- server.py 服务器
- application.py 用于实例化对象
  - Application 继承自tornado.web.Application
- config.py 参数以及配置
- static 静态文件目录
- templates 模板目录
- views 视图目录
  - index.py 视图类
    - IndexHandler  (控制访问 / )
    - HomeHandler  (控制访问 /home )

1. 启动server.py
2. 在浏览器输入127.0.0.1:9000
    127.0.0.1:9000/home